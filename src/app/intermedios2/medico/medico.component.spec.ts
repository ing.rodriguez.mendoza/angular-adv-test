import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MedicoComponent } from './medico.component';
import { MedicoService } from './medico.service';
import { HttpClientModule } from '@angular/common/http';

describe('Medico Component', () => { 

    let component: MedicoComponent;
    let fixture: ComponentFixture<MedicoComponent>;

    beforeEach( () => { 
      
        //el TestBed permite crear un componente
        TestBed.configureTestingModule({
            declarations: [ MedicoComponent ],
            imports: [ HttpClientModule ],
            providers: [ MedicoService ]
        });

        fixture = TestBed.createComponent( MedicoComponent );
        component = fixture.componentInstance;

    } );

    it('Debe de crearse el componente', () => { 
        expect( component ).toBeTruthy();
    });

    it('Debe de retornar el nombre del medico', () => { 

        const medico = 'Diana Ysabel';
        
        const resp = component.saludarMedico(medico);

        expect( resp ).toContain( medico );

    });
})