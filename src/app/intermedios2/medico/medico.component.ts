import { Component, OnInit } from '@angular/core';
import { MedicoService } from './medico.service';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styles: [
  ]
})
export class MedicoComponent implements OnInit {

  medicos: any[] = [];

  constructor(public medicoService: MedicoService) { }

  ngOnInit(): void {
  }

  saludarMedico(nombre: string){
    return `hola doc ${nombre}`
  }

  obtenerMedicos(){
    return this.medicoService.getMedicos()
    .subscribe( (med:any) => this.medicos = med )
  }

}
