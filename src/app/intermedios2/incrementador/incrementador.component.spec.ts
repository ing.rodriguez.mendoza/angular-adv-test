import { TestBed, ComponentFixture, waitForAsync } from '@angular/core/testing';
import { IncrementadorComponent } from './incrementador.component';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';


describe('Incremendator Component', () => {

    let component: IncrementadorComponent;
    let fixture: ComponentFixture<IncrementadorComponent>;

    beforeEach( () => {
        TestBed.configureTestingModule({
            declarations: [ IncrementadorComponent ],
            imports: [ FormsModule ]
        });

        fixture = TestBed.createComponent(IncrementadorComponent);
        component = fixture.componentInstance;

    });

    it('Probando si existe el incrementador', () => {

        component.leyenda = 'Progreso de carga';

       fixture.detectChanges();

        const element : HTMLElement = fixture.debugElement.query( By.css('h3') ).nativeElement;

        expect( element.innerHTML ).toContain( component.leyenda );

    });

    
    // it('Debe mostrar en el input el valor del progreso', async() => 
    //     {
    //         component.cambiarValor(5);
    //         fixture.detectChanges();
    //         await fixture.whenStable().then(() => {
    //         const elem: HTMLInputElement = fixture.debugElement.query(By.css('input')).nativeElement;
    //         expect(elem.valueAsNumber).toBe(55);
    //         });
    //     }
    // );

    
    it('Mostrar en input valor del progreso', waitForAsync(() => {
 
        component.cambiarValor(5);
        fixture.detectChanges();
 
        fixture.whenStable().then(() => {
            const input = fixture.debugElement.query(By.css('input'));
            const elm: HTMLInputElement = input.nativeElement;
 
            // console.log(elm);
 
            expect(elm.value).toBe('55');
        })
 
    }));

    it('Debe incrementar/decrementar en 5, con un click en el botón', () => { 

        const botones = fixture.debugElement.queryAll( By.css('.btn-primary') );
        // console.log(botones);
        
        botones[0].triggerEventHandler('click',null);
        //al realizar click el boton botones[0] decrementa
        expect(component.progreso).toBe(45);

        botones[1].triggerEventHandler('click',null);
        //al realizar click el boton botones[1] incrementa
        expect(component.progreso).toBe(50);

    });

    it('Debe decrementar en 5, con un click en el botón', () => { 

        const botones = fixture.debugElement.queryAll( By.css('.btn-primary') );
        // console.log(botones);
        
        botones[0].triggerEventHandler('click',null);
        //al realizar click el boton botones[0] decrementa

        fixture.detectChanges();

        const element : HTMLElement = fixture.debugElement.query( By.css('h3') ).nativeElement;

        expect( element.innerHTML ).toContain('45');

    });
});
