import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { IncrementadorComponent } from "./incrementador.component";


describe('Incrementador Component Unit', () => { 

    //#region cabecera basico para pruebas de componentes
    let component: IncrementadorComponent;
    let fixture: ComponentFixture<IncrementadorComponent>;

    beforeEach( () => {
        TestBed.configureTestingModule({
            declarations: [ IncrementadorComponent ],
            imports: [ FormsModule ]
        });

        fixture = TestBed.createComponent(IncrementadorComponent);
        component = fixture.componentInstance;

    });
    //#endregion

    it('No debe de pasar de 100 el progreso', () => { 
        // const element: HTMLElement = fixture.debugElement.query( By.css('h3') ).nativeElement;
        // expect( element.innerHTML ).not.toBeGreaterThan(100)
        component.progreso = 50;
        component.cambiarValor(5);

        expect( component.progreso ).toBe(55);
    });

});