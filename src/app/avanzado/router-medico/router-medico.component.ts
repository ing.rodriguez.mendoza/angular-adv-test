import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-router-medico',
  templateUrl: './router-medico.component.html',
  styleUrls: ['./router-medico.component.css']
})
export class RouterMedicoComponent implements OnInit {

  id: string | undefined;

  constructor(
    private router:Router,
    private activateddRouter: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.activateddRouter.params
      .subscribe( params => this.id = params['id'] );

  }

  guardarMedico() {
    this.router.navigate(['medico','123'])
  }

}
