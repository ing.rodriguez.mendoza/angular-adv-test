import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterMedicoComponent } from './router-medico.component';
import { Observable, EMPTY, Subject, of, from } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

class FakeRouter {
  navigate( params: any ){}
}

class FakeActivatedRoute{
  // params: Observable<any> = EMPTY;

  //pra poder insertar valores a un observable podemos trabajar con Subject
  private subject = new Subject();

  push( valor: any ){
    this.subject.next( valor );
  }
  
  get params() {
    return this.subject.asObservable();
  }
  

}

describe('RouterMedicoComponent', () => {
  let component: RouterMedicoComponent;
  let fixture: ComponentFixture<RouterMedicoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RouterMedicoComponent ],
      providers: [
        //reemplazando el uso de la clase Router por FakeRouter
        { provide: Router, useClass: FakeRouter },
        // //reemplazando el uso de la clase ActivatedRoute por FakeActivatedRoute
        // { provide: ActivatedRoute, useClass: FakeActivatedRoute },
        { provide: ActivatedRoute, useValue: { params: from([{id:'nuevo'}]) } }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterMedicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe de redireccionar a Médico cuando se guarde', () => { 
     
     const router = TestBed.inject(Router);
     //vamos espiar al router a traves del metodo navigate que este contiene
     const spy = spyOn( router, 'navigate' );

     component.guardarMedico();

     expect( spy ).toHaveBeenCalledWith( ['medico', '123'] );

  });

  it('Debe de colocar el id = nuevo', () => { 

    component = fixture.componentInstance;
    //const activatedRoute: FakeActivatedRoute = TestBed.inject(ActivatedRoute);

    //activatedRoute.push( {id: 'nuevo'} );

    expect( component.id ).toBe('nuevo');

  });

});
