import { usuarioIngresado } from './booleanos';

xdescribe('Pruebas de Booleanos', () => {

    it('Debe de retornar true', () => {
        const resp = usuarioIngresado();
        //si nuestra funcion retorna siempre false
        // expect(resp).toBeFalsy();
        //si nuestra funcion retorna siempre true
        expect(resp).toBeTruthy();
    });

});