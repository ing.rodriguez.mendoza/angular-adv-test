import { obtenerRobot } from './arreglos';

describe('Pruebas de Arreglos', () => { 
    
    it('Debe mostrar por lo mínimo 4 robots', ()=> {
        const resp = obtenerRobot();
        expect(resp.length).toBeGreaterThanOrEqual(4);
        // expect(resp).toHaveSize(4);
    });

    //x para que no se tome en cuenta
    xit('Debe existir Megaman y Ultron', ()=> {
        const resp = obtenerRobot();
        expect(resp).toContain('Megaman');
        expect(resp).toContain('Ultron');
    });

})