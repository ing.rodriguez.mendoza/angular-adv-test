import { Jugador } from './clase';

describe('Prueba de clases', () => { 
    
    let jugador = new Jugador;

    beforeAll( () => { 
    // console.log('beforeAll')
    });

    beforeEach( () => { 
        //console.log('beforeEach')
        // jugador.hp = 100;
        jugador = new Jugador;
    });

    afterAll( () => { 
        //console.log('afterAll')
    });

    afterEach( () => { 
        //console.log('afterEach')
        // jugador.hp = 100;
    });

    it('Debe retornar 80 de hp, si recibe 20 de daño', () => { 
        const resp = jugador.recibiendoDanio(20);
        const hp = jugador.hp;
        expect(resp).toBe(80);
    });
    
    it('Debe retornar 40 de hp, si recibe 60 de daño', () => { 
        console.log('Esta prueba va fallar')
        const resp = jugador.recibiendoDanio(60);
        const hp = jugador.hp;
        expect(resp).toBe(40);
    });

    it('Debe retornar 0 de hp, si recibe 100 de daño', () => { 
        console.log('Esta prueba va fallar')
        const resp = jugador.recibiendoDanio(100);
        const hp = jugador.hp;
        expect(resp).toBe(0);
    });
})