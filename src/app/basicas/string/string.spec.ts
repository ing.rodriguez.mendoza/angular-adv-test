import { mensaje } from './string';
// describe('Pruebas de String');
// it('Debe de regresar un string');
// it('Debe de contener un nombre');

xdescribe('Pruebas de Strings', () => { 
    it('Debe de regresar un string', () => {
        const resp = mensaje('Diana Ysabel');
        //puede ser multiples expect en un it
        //toBe me ayuda a comparar
        expect(typeof resp).toBe('string')
    });

    it('Debe de regresar un saludo con el nombre enviado', () => {
        
        const nombre = 'Mariana'
        const resp = mensaje( nombre );
        //puede ser multiples expect en un it
        //toBe me ayuda a comparar
        expect(resp).toContain( nombre );
    });
});