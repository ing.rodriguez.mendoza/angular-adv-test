import { incrementar } from './numeros';

describe('Pruebas de numeros', () => {
    it('Debe retornar 100, si el número es mayor a 100', () => {
        const resp = incrementar(300);
        expect( resp ).toBe( 100 );
    });

    it('Debe incrementar en uno, si el número es menor a 100', () => {
        const resp = incrementar(60);
        expect( resp ).toBe( 61 );
    });
});