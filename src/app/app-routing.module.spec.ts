import { Router } from "@angular/router";
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from "./app-routing.module";
import { HospitalComponent } from './intermedios2/hospital/hospital.component';
import { MedicoComponent } from './intermedios2/medico/medico.component';
import { MedicoService } from './intermedios2/medico/medico.service';
import { HttpClientModule } from '@angular/common/http';

describe('Rutas principales', () => {

    let location: Location;
    let router: Router;
    let fixture;    

    beforeEach(() => { 
        TestBed.configureTestingModule({
            imports:[RouterTestingModule.withRoutes(routes), HttpClientModule],
            declarations: [
                HospitalComponent,
                MedicoComponent
            ],
            // providers: [
            //     MedicoService
            // ],
        });

        router = TestBed.get(Router);
        // location = TestBed.get(Location);
        fixture = TestBed.createComponent(MedicoComponent);
        router.initialNavigation();
    })

    it('Debe de existir la ruta /medico/:id', fakeAsync(() => { 
        router.navigate(['']);
        tick();
        expect(routes).toContain({
            path: 'medico/:id',
            component: MedicoComponent
        });
    }));

    
    // it('navigates to another route when (...)', () => {
    //     // The type annotation can be left out as it's inferred from `TestBed.inject`
    //     const location: Location = TestBed.inject(Location);

    //     // (...)

    //     // expect(location.path()).toBe('/some/other/route');
    // });


 });