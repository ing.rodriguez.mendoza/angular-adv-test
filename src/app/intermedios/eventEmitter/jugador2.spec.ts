import { Jugador2 } from './jugador2';


describe('Jugador 2', () => {
    let jugador = new Jugador2;

    beforeEach( () => {
        console.log('entro al beforeEach');
        jugador = new Jugador2();
    } );

    it('Debe emitir un evento cuando el jugador recibe daño', () => { 

        let nuevoHp = 0;

        jugador.hpCambia.subscribe( (hp) => { nuevoHp = hp  });

        jugador.recibeDanio(1000);
        
        expect( nuevoHp ).toBe(0);
    });

    it('Debe emitir un evento cuando el jugador recibe daño y sobrevivir si es menos de 100', () => { 

        let nuevoHp = 0;

        jugador.hpCambia.subscribe( (hp) => { nuevoHp = hp  });

        jugador.recibeDanio(80);
        
        expect( nuevoHp ).toBe(20);
    });

    
});