import { FormBuilder } from '@angular/forms';
import { FormularioRegister } from './formularios';


describe('', () => { 

    let componente: FormularioRegister;

    beforeEach( () => {
        componente = new FormularioRegister( new FormBuilder );
    });

    it('Debe crear un formulario con dos campos email y password', () => { 
        expect(componente.form.contains('email')).toBeTruthy();
        expect(componente.form.contains('password')).toBeTruthy();
    });

    it('El email debe ser obligatorio', () => { 
        const email = componente.form.get('email');
        email?.setValue('d');
        expect(email?.errors).toBeFalsy();
    });
})