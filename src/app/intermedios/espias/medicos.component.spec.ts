import { EMPTY, from, Observable, throwError } from 'rxjs';
import { MedicosComponent } from './medicos.component';
import { MedicosService } from './medicos.service';

describe('MedicosComponent', () => {

    let componente: MedicosComponent;
    const servicio = new MedicosService(null);

    beforeEach( () => { 
        componente = new MedicosComponent(servicio);
    } );


    // const arrMed = ['',''];
    it('Init: Debe de cargar los médicos', () => {
        
        spyOn( servicio, 'getMedicos' ).and.callFake( () => { 
            const arrMed = ['Medico01','Medico02','Medico03'];
            return from( [ arrMed ] );
        } );

        componente.ngOnInit();

        expect( componente.medicos.length ).toBeGreaterThan(1);
    });

    it('Debe de llamar al servidor para agregar un médico', () => { 
        // const espia = spyOn( servicio, 'agregarMedico').and.callFake( () => new Observable());
        const espia = spyOn( servicio, 'agregarMedico').and.callFake( () => EMPTY);

        componente.agregarMedico();

        expect( espia ).toHaveBeenCalled();
    });

    it('Debe de agregar un nuevo médico al arreglo de médicos', () => { 

        const medico = { id: 1, nombre: 'Gerardo'};
        //retornamos un observable
        spyOn( servicio, 'agregarMedico').and.returnValue( from([medico]) );

        componente.agregarMedico();

        expect( componente.medicos.indexOf( medico ) ).toBeGreaterThanOrEqual(0);

    });

    it('Si falla la adición, la propiedad mensajeError, debe ser igual al error del servicio', () => { 

        const miError = 'No se puede agregar médico';
        //disparamos un error
        spyOn( servicio, 'agregarMedico').and.returnValue( throwError( () => miError ) );

        componente.agregarMedico();

        expect( componente.mensajeError ).toBe( miError );

    });

    it('Debe de llamar al servidor para borrar un médico', () => { 
        
        spyOn( window, 'confirm' ).and.returnValue(true);

        const item = { id: '1', nombre: 'ddd' }
        const espia = spyOn( servicio, 'borrarMedico').and.returnValue(EMPTY);

        componente.borrarMedico(item.id);

        expect( espia ).toHaveBeenCalledWith(item.id);
    });

    it('NO debe de llamar al servidor para borrar un médico', () => { 
        
        spyOn( window, 'confirm' ).and.returnValue(false);

        const item = { id: '1', nombre: 'ddd' }
        const espia = spyOn( servicio, 'borrarMedico').and.returnValue(EMPTY);

        componente.borrarMedico(item.id);

        expect( espia ).not.toHaveBeenCalledWith(item.id);
    });
});
