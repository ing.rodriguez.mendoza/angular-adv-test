import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HospitalComponent } from './intermedios2/hospital/hospital.component';
import { MedicoComponent } from './intermedios2/medico/medico.component';
import { IncrementadorComponent } from './intermedios2/incrementador/incrementador.component';

export const routes: Routes = [
  { path: 'hospital',   component: HospitalComponent },
  { path: 'medico/:id', component: MedicoComponent },
  { path: '**',         component: IncrementadorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
